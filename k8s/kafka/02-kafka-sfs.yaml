apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: kafka
  labels:
    app.kubernetes.io/name: kafka
spec:
  selector:
    matchLabels:
      app.kubernetes.io/name: kafka
  serviceName: kafka-headless
  podManagementPolicy: OrderedReady
  updateStrategy:
    type: OnDelete
  replicas: 1
  template:
    metadata:
      labels:
        helm.sh/chart: kafka-0.21.0
        app.kubernetes.io/name: kafka
    spec:
      terminationGracePeriodSeconds: 10
      initContainers:
        - name: wait-for-zookeeper
          image: alpine:3.12.0
          command: [ 'sh', '-c', "until nslookup zookeeper.$(cat /var/run/secrets/kubernetes.io/serviceaccount/namespace).svc.cluster.local; do echo waiting for zookeeper; sleep 2; done" ]
      containers:
        - name: kafka-broker
          image: "confluentinc/cp-kafka:5.0.1"
          imagePullPolicy: "IfNotPresent"
          livenessProbe:
            exec:
              command:
                - sh
                - -ec
                - /usr/bin/jps | /bin/grep -q SupportedKafka
            initialDelaySeconds: 30
            timeoutSeconds: 5
          readinessProbe:
            tcpSocket:
              port: kafka
            initialDelaySeconds: 30
            periodSeconds: 10
            timeoutSeconds: 5
            successThreshold: 1
            failureThreshold: 3
          ports:
            - containerPort: 9092
              name: kafka
            - containerPort: 9093
            - containerPort: 9094
          resources:
            requests:
              memory: "512Mi"
            limits:
              memory: "1024Mi"
          env:
            - name: POD_IP
              valueFrom:
                fieldRef:
                  fieldPath: status.podIP
            - name: POD_NAME
              valueFrom:
                fieldRef:
                  fieldPath: metadata.name
            - name: POD_NAMESPACE
              valueFrom:
                fieldRef:
                  fieldPath: metadata.namespace
            - name: KAFKA_HEAP_OPTS
              value: -Xmx1G -Xms1G
            - name: KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR
              value: "1"
            - name: KAFKA_ZOOKEEPER_CONNECT
              value: "zookeeper-headless:2181"
            - name: KAFKA_LOG_DIRS
              value: "/opt/kafka/data/logs"
            - name: "KAFKA_CONFLUENT_SUPPORT_METRICS_ENABLE"
              value: "false"
            - name: "KAFKA_AUTO_CREATE_TOPICS_ENABLE"
              value: "true"
            - name: "KAFKA_CONFIG_STORAGE_REPLICATION_FACTOR"
              value: "1"
            - name: "KAFKA_CONFLUENT_METRICS_REPORTER_TOPIC_REPLICAS"
              value: "1"
            - name: KAFKA_LISTENERS
              value: "CLIENT://:9092,INTERNAL://:9093,EXTERNAL://localhost:9094"
            - name: KAFKA_ADVERTISED_LISTENERS
              value: "CLIENT://:9092,INTERNAL://:9093,EXTERNAL://localhost:9094"
            - name: KAFKA_LISTENER_SECURITY_PROTOCOL_MAP
              value: "INTERNAL:PLAINTEXT,CLIENT:PLAINTEXT,EXTERNAL:PLAINTEXT"
            - name: KAFKA_LISTENERS
              value: "CLIENT://:9092,INTERNAL://:9093,EXTERNAL://localhost:9094"
            - name: KAFKA_ADVERTISED_LISTENERS
              value: "CLIENT://:9092,INTERNAL://:9093,EXTERNAL://localhost:9094"
            - name: KAFKA_INTER_BROKER_LISTENER_NAME
              value: CLIENT
            - name: "KAFKA_DEFAULT_REPLICATION_FACTOR"
              value: "1"
            - name: "KAFKA_MIN_INSYNC_REPLICAS"
              value: "1"
            - name: "KAFKA_OFFSET_STORAGE_REPLICATION_FACTOR"
              value: "1"
            - name: "KAFKA_STATUS_STORAGE_REPLICATION_FACTOR"
              value: "1"
            - name: KAFKA_JMX_PORT
              value: "5555"
          volumeMounts:
            - name: init-topics
              mountPath: /opt/init-scripts/
          # This is required because the Downward API does not yet support identification of
          # pod numbering in statefulsets. Thus, we are required to specify a command which
          # allows us to extract the pod ID for usage as the Kafka Broker ID.
          # See: https://github.com/kubernetes/kubernetes/issues/31218
          command:
            - sh
            - -exc
            - |
              unset KAFKA_PORT && \
              export KAFKA_BROKER_ID=${POD_NAME##*-} && \
              exec /etc/confluent/docker/run
          lifecycle:
            postStart:
              exec:
                command:
                  - sh
                  - -exc
                  - |
                    until /opt/init-scripts/initTopics.sh
                    do
                    sleep 1
                    done
      volumes:
        - name: init-topics
          configMap:
            name: init-topics
            defaultMode: 0777