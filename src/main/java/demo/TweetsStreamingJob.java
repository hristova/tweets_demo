package demo;

import demo.functions.StateFlatMapFunction;
import demo.functions.TimeToDeleteCoFlatMapFunction;
import demo.tweet.DeletedTweet;
import demo.tweet.Tweet;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.stream.StreamSupport;

import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.collector.selector.OutputSelector;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.SplitStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.windowing.AllWindowFunction;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.streaming.connectors.twitter.TwitterSource;
import org.apache.flink.util.Collector;


public class TweetsStreamingJob {

    public static void main(String[] args) throws Exception {
        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setStreamTimeCharacteristic(TimeCharacteristic.IngestionTime);

        final Properties props = new Properties();
        props.setProperty(TwitterSource.CONSUMER_KEY, "GXBk71BlllmS6lWvLVGVZ6Wyv");
        props.setProperty(TwitterSource.CONSUMER_SECRET, "aprxtPpiwiLasbvmodpR5qo95q2z5fO4NBv5PSiZILmP2nm6Em");
        props.setProperty(TwitterSource.TOKEN, "974014889336426496-CVmPGENsv1G7EYSdJuDtYsiEk02JpEm");
        props.setProperty(TwitterSource.TOKEN_SECRET, "bw0paLXyIKOF9w2aEyFPjdWIujY2kFPXu7SRzZVW4hvwc");

        final DataStream<String> originalTweets = env.addSource(new TwitterSource(props));
        originalTweets.print();

        // window, number of tweets in each language
        final DataStream<Tuple3<String, Long, Date>> tweetCountPerLang = originalTweets
                .map(Tweet::parse)
                .filter(t -> t.getLang() != null)
                .keyBy(Tweet::getLang)
                .timeWindow(Time.minutes(1))
                .apply((String pLang, TimeWindow pTimeWindow, Iterable<Tweet> pIterable, Collector<Tuple3<String,
                        Long, Date>> pCollector) -> pCollector
                        .collect(new Tuple3<>(pLang, StreamSupport.stream(pIterable.spliterator(), false).count(),
                                new Date(pTimeWindow.getEnd()))));
        tweetCountPerLang.print();

        // most tweeted hashtag
        // all tweets stream -> all tags stream -> #Sbtech site  ->
        //                                      -> ...           -> all tags with counts
        //                                      -> #Flink stream ->
        final DataStream<Tuple3<Date, String, Integer>> topHashtag = originalTweets.map(Tweet::parse)
                // <hashtag name, integer>
                .flatMap((Tweet pTweet, Collector<Tuple2<String, Integer>> pCollector) -> pTweet.getHashtags()
                        // for summing later
                        .forEach(hashtag -> pCollector.collect(new Tuple2<>(hashtag, 1))))
                .keyBy(0)
                .timeWindow(Time.minutes(10))
                .sum(1) // summin all 1s for all tuples with hashtag and 1s
                .timeWindowAll(Time.minutes(10)) // non - keyed window for all summed tuples, to find top hashtag
                .apply(new AllWindowFunction<Tuple2<String, Integer>, Tuple3<Date, String, Integer>, TimeWindow>() {

                    @Override
                    public void apply(final TimeWindow pTimeWindow, final Iterable<Tuple2<String, Integer>> pIterable,
                            final Collector<Tuple3<Date, String, Integer>> pCollector) throws Exception {

                        final Tuple2<String, Integer> topTag = StreamSupport
                                .stream(pIterable.spliterator(), false)
                                .max(Comparator.comparingInt(pT -> pT.f1))
                                .orElse(new Tuple2<>("", 0));

                        pCollector.collect(new Tuple3<>(new Date(pTimeWindow.getEnd()), topTag.f0, topTag.f1));
                    }
                });
        topHashtag.print();

        // unique user names and most retweeted
        final DataStream<String> tweets = originalTweets
                .map(Tweet::parse)
                .filter(pTweet -> pTweet.getId() != null && pTweet.getLang() != null)
                .keyBy(Tweet::getLang)
                .flatMap(new StateFlatMapFunction());
        tweets.print();


        // splitting original stream
        final SplitStream<String> tweetSplitIsDeleted = originalTweets.split((OutputSelector<String>) pValue -> {
            final List<String> output = new ArrayList<>();
            if (pValue.contains("delete")) {
                output.add("deleted");
            } else {
                output.add("notDeleted");
            }
            return output;
        });


        // connect two data streams by id
        final DataStream<String> deleted = tweetSplitIsDeleted.select("deleted");
        final DataStream<String> notDeleted = tweetSplitIsDeleted.select("notDeleted");
        deleted.print();
        notDeleted.print();
        final DataStream<Long> daysOfDeletion = deleted
                .map(DeletedTweet::new)
                .filter(del -> del.getId() != null)
                .keyBy(DeletedTweet::getId)
                .connect(notDeleted.map(Tweet::parse).filter(notDel -> notDel.getId() != null).keyBy(Tweet::getId))
                .flatMap(new TimeToDeleteCoFlatMapFunction());
        daysOfDeletion.print();
        env.execute("Flink-Tweet-Streaming-Api");
    }
}
