package demo.tweet;

import java.io.IOException;
import java.util.Objects;

import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.JsonNode;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Deleted tweet.
 *
 * @author monika.hristova
 */
public class DeletedTweet {

    private transient ObjectMapper jsonParser;

    private final String originalTweet;

    private Long id;

    private Long timestamp;



    public DeletedTweet(final String pOriginalTweet) {
        originalTweet = pOriginalTweet;
        parse(originalTweet);
    }



    private void parse(final String value) {
        if (jsonParser == null) {
            jsonParser = new ObjectMapper();
        }
        JsonNode jsonNode;
        try {
            jsonNode = jsonParser.readValue(value, JsonNode.class);
            if (jsonNode.has("delete")) {
                id = jsonNode.get("delete").get("status").get("id").asLong();
                timestamp = jsonNode.get("delete").get("timestamp_ms").asLong();
            }
        }
        catch (IOException pE) {
            // ignore
        }
    }



    public Long getId() {
        return id;
    }



    public void setId(final Long pId) {
        id = pId;
    }



    public Long getTimestamp() {
        return timestamp;
    }



    public void setTimestamp(final Long pTimestamp) {
        timestamp = pTimestamp;
    }



    @Override
    public boolean equals(final Object pO) {
        if (this == pO)
            return true;
        if (!(pO instanceof DeletedTweet))
            return false;
        final DeletedTweet that = (DeletedTweet) pO;
        return Objects.equals(id, that.id) && Objects.equals(timestamp, that.timestamp);
    }



    @Override
    public int hashCode() {
        return Objects.hash(jsonParser, originalTweet, id, timestamp);
    }



    @Override
    public String toString() {
        return new org.apache.commons.lang3.builder.ToStringBuilder(this)
                .append("id", id)
                .append("timestamp", timestamp)
                .toString();
    }
}
