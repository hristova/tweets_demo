package demo.tweet;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.JsonNode;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Tweet class.
 *
 * @author monika.hristova
 */
public class Tweet implements Serializable {

    private Long id;

    private String user;

    private String lang;

    private String tweet;

    private Long timestamp;

    private int retweeted;

    private static ObjectMapper jsonParser;

    private String originalTweet;

    private List<String> hashtags;



    public Tweet() {

    }



    public static Tweet parse(final String value) {
        if (jsonParser == null) {
            jsonParser = new ObjectMapper();
        }
        JsonNode jsonNode;
        final Tweet tweetObj = new Tweet();
        try {
            jsonNode = jsonParser.readValue(value, JsonNode.class);
            if (jsonNode.has("id")) {
                tweetObj.id = jsonNode.get("id").asLong();
            }
            if (jsonNode.has("timestamp_ms")) {
                tweetObj.timestamp = jsonNode.get("timestamp_ms").asLong();
            }
            if (jsonNode.has("text")) {
                tweetObj.tweet = jsonNode.get("text").asText();
            }
            if (jsonNode.has("user")) {
                tweetObj.user = jsonNode.get("user").get("name").asText();
            }
            if (jsonNode.has("lang")) {
                tweetObj.lang = jsonNode.get("lang").asText();
            }
            if (jsonNode.has("retweeted_status")) {
                tweetObj.retweeted = jsonNode.get("retweeted_status").get("retweet_count").asInt();
            }
            if (jsonNode.has("entities")) {
                final JsonNode tags = jsonNode.get("entities").get("hashtags");
                tags
                        .elements()
                        .forEachRemaining(tagNode -> tweetObj.getHashtags().add(tagNode.get("text").textValue()));
            }
        }
        catch (IOException pE) {
            // ignore
        }
        return tweetObj;
    }



    public Long getId() {
        return id;
    }



    public void setId(final Long pId) {
        id = pId;
    }



    public String getTweet() {
        return tweet;
    }



    public void setTweet(final String pTweet) {
        tweet = pTweet;
    }



    public Long getTimestamp() {
        return timestamp;
    }



    public void setTimestamp(final Long pTimestamp) {
        timestamp = pTimestamp;
    }



    public String getOriginalTweet() {
        return originalTweet;
    }



    public void setOriginalTweet(final String pOriginalTweet) {
        originalTweet = pOriginalTweet;
    }



    public String getUser() {
        return user;
    }



    public void setUser(final String pUser) {
        user = pUser;
    }



    public String getLang() {
        return lang;
    }



    public void setLang(final String pLang) {
        lang = pLang;
    }



    public int getRetweeted() {
        return retweeted;
    }



    public void setRetweeted(final int pRetweeted) {
        retweeted = pRetweeted;
    }



    public List<String> getHashtags() {
        if (hashtags == null) {
            hashtags = new ArrayList<>();
        }
        return hashtags;
    }



    public void setHashtags(final List<String> pHashtags) {
        hashtags.clear();
        getHashtags().addAll(pHashtags);
    }



    @Override
    public boolean equals(final Object pO) {
        if (this == pO) {
            return true;
        }
        if (!(pO instanceof Tweet)) {
            return false;
        }
        final Tweet tObj = (Tweet) pO;
        return Objects.equals(id, tObj.id) && Objects.equals(tweet, tObj.tweet) && Objects.equals(timestamp,
                tObj.timestamp);
    }



    @Override
    public int hashCode() {
        return Objects.hash(id, tweet, timestamp);
    }



    @Override
    public String toString() {
        return new org.apache.commons.lang3.builder.ToStringBuilder(this)
                .append("id", id)
                .append("user", user)
                .append("lang", lang)
                .append("tweet", tweet)
                .append("timestamp", timestamp)
                .append("originalTweet", originalTweet)
                .append("jsonParser", jsonParser)
                .toString();
    }
}
