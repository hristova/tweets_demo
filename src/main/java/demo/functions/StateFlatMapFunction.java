package demo.functions;

import demo.tweet.Tweet;

import java.util.Optional;
import java.util.stream.StreamSupport;

import org.apache.flink.api.common.functions.RichFlatMapFunction;
import org.apache.flink.api.common.state.ListState;
import org.apache.flink.api.common.state.ListStateDescriptor;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.util.Collector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Unique Function
 *
 * @author monika.hristova
 */
public class StateFlatMapFunction extends RichFlatMapFunction<Tweet, String> {

    private static final Logger LOGGER = LoggerFactory.getLogger(StateFlatMapFunction.class);

    private transient ListState<String> uniqueUsers;

    private transient ValueState<Tweet> mostRetweeted;



    @Override
    public void open(final Configuration pParameters) throws Exception {
        super.open(pParameters);

        uniqueUsers = getRuntimeContext().getListState(new ListStateDescriptor<>("usernames", String.class));
        mostRetweeted = getRuntimeContext().getState(new ValueStateDescriptor<>("most_retweeted", Tweet.class));
    }



    @Override
    public void flatMap(final Tweet pTweet, final Collector<String> pCollector) throws Exception {
        final Tweet mostRetweetedByLang = mostRetweeted.value();
        if (mostRetweetedByLang == null || mostRetweetedByLang.getRetweeted() < pTweet.getRetweeted()) {
            LOGGER.info(
                    "Current number of retweeting of most retweeted tweet for language --> {} <-- is {} and it will be "
                            + "updated with {}.", pTweet.getLang().toUpperCase(),
                    mostRetweetedByLang != null ? mostRetweetedByLang.getRetweeted() : 0, pTweet.getRetweeted());
            mostRetweeted.update(pTweet);
        } else {
            LOGGER.info(
                    "Current number of {} retweeting is the most retweeted tweet for language --> {} <-- up to now.",
                    mostRetweetedByLang.getRetweeted(), pTweet.getLang().toUpperCase());
        }

        final Optional<String> any = StreamSupport
                .stream(uniqueUsers.get().spliterator(), false)
                .filter(users -> users.equals(pTweet.getUser()))
                .findAny();
        if (!any.isPresent()) {
            uniqueUsers.add(pTweet.getUser());
            pCollector.collect(pTweet.getUser());
        }
    }
}
