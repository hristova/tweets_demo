package demo.functions;

import demo.tweet.DeletedTweet;
import demo.tweet.Tweet;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.TimeZone;

import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.co.RichCoFlatMapFunction;
import org.apache.flink.util.Collector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Measures how long it takes a tweet to be deleted.
 *
 * @author monika.hristova
 */
public class TimeToDeleteCoFlatMapFunction extends RichCoFlatMapFunction<DeletedTweet, Tweet, Long> {

    private transient ValueState<Tweet> created;

    private static final Logger LOGGER = LoggerFactory.getLogger(StateFlatMapFunction.class);



    @Override
    public void open(final Configuration pParameters) throws Exception {
        super.open(pParameters);

        created = getRuntimeContext().getState(new ValueStateDescriptor<>("createdTweet", Tweet.class));
    }



    @Override
    public void flatMap1(final DeletedTweet pTweet, final Collector<Long> pCollector) throws Exception {
        if (created.value() != null) {
            LOGGER.info("Tweet deleted");
            LocalDateTime toDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(created.value().getTimestamp()),
                    TimeZone.getTimeZone("UTC").toZoneId());
            LocalDateTime fromDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(pTweet.getTimestamp()),
                    TimeZone.getTimeZone("UTC").toZoneId());
            pCollector.collect(ChronoUnit.DAYS.between(fromDateTime, toDateTime));
        }
    }



    @Override
    public void flatMap2(final Tweet pTweet, final Collector<Long> pCollector) throws Exception {
        LOGGER.info("Tweet created.");
        created.update(pTweet);
    }
}
